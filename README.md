# Python interfaces for TA1 primitives

A collection of Python interfaces for TA1 primitives.

See [base.py](https://gitlab.com/datadrivendiscovery/primitive-interfaces/blob/devel/primitive_interfaces/base.py)
for the unified base class and description of the base class interface and available base mixins.
Other files in that module provide a range of specialized interfaces by subclassing the base class.
The purposes of non-base interfaces are:

* they can declare stricter or specialized input and output types
* they can signal to TA2 systems which primitives are doing what on a different level
  than just through input and output types
* other primitives can declare that they are accepting as an argument to their methods
  just a subset of primitives using that interface class as a type of the argument
  (when a primitive is used as a value to a method itself)
* can be used a workaround where input and output types are not enough

[Primitives Annotation Schema](https://datadrivendiscovery.org/wiki/display/gov/Primitives+Annotation+Schema)
defines `interfaces` field, which should be a list of base classes and mixins used by the primitive in their
method resolution order (MRO).
The idea is that each of those `<module>.<class>` values map to a `primitive_interfaces.<module>.<class>`
Python class, which is an abstract class, the interface for that `interfaces` value. Moreover, `interfaces_version`
tells the version of this package you used when generating the annotation (`primitive_interfaces.__version__`).

See also [`d3m`](https://gitlab.com/datadrivendiscovery/d3m) package which provides some utility functions,
like listing all locally installed primitive and generating JSON annotations for them automatically.

## About Data Driven Discovery Program

DARPA Data Driven Discovery (D3M) Program is researching ways to get machines to build
machine learning pipelines automatically. It is split into three layers:
TA1 (primitives), TA2 (systems which combine primitives automatically into pipelines
and executes them), and TA3 (end-users interfaces).

## Installation

This package works with Python 3.6+.

You can run
```
pip install -r requirements.txt
pip install .
```
in the root directory of this repository to install the `primitive_interfaces` package.

## Changelog

See [HISTORY.md](./HISTORY.md) for summary of changes to this package.

## Primitives D3M namespace

Using the [`d3m`](https://gitlab.com/datadrivendiscovery/d3m) package, the `d3m.primitives` module exposes all
primitives under the same `d3m.primitives` namespace.

This is achieved using [Python entry points](https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins).
Python packages containing primitives should register them and expose them under the common
namespace by adding an entry like the following to package's `setup.py`:

```python
entry_points = {
    'd3m.primitives': [
        'primitive_namespace.PrimitiveName = my_package.my_module:PrimitiveClassName',
    ],
},
```

The example above would expose the `my_package.my_module.PrimitiveClassName` primitive under
`d3m.primitives.primitive_namespace.PrimitiveName`.

Configuring `entry_points` in your `setup.py` does not just put primitives into a common namespace, but also
helps with discovery of your primitives on the system. Then your package with primitives just have to be
installed on the system and can be automatically discovered and used by any other Python code.

See [`d3m` package](https://gitlab.com/datadrivendiscovery/d3m) for more details.
One does not have to use `d3m` package to make use of these entry points. You can consume them
yourself directly. (But `d3m` package exposes a
[thin API on top](https://gitlab.com/datadrivendiscovery/d3m#index) to make it easier for you.)

## Metadata

You can put primitive's annotation for your primitive directly into code. This allows users of your
primitive to know what is it's annotation by just expecting the code instead of having to match a
JSON annotation with the primitive.

To annotate the primitive in code, simply declare a `metadata` attribute on your class, containing
the annotation according to [Primitives Annotation Schema](https://datadrivendiscovery.org/wiki/display/gov/Primitives+Annotation+Schema).
You can also declare `__author__` on the class, which will be used for `author` field in the annotation
(and the `team` field in the annotation, if not provided otherwise).

If you have multiple primitives in your package and some part of metadata is the same for your primitives,
you can also define `metadata` at the package level (and `__author__`). Primitive level has precedence
over the package level metadata, so you can also override defaults at the primitive level.

See [`common-primitives` package](https://gitlab.com/datadrivendiscovery/common-primitives/blob/devel/common_primitives/__init__.py)
and [primitive class](https://gitlab.com/datadrivendiscovery/common-primitives/blob/devel/common_primitives/random_forest/random_forest.py) for an example.

This metadata (together with typing information) can be used also to
[automatically generate JSON](https://gitlab.com/datadrivendiscovery/d3m#automatic-annotation-generation).
See [`d3m` package](https://gitlab.com/datadrivendiscovery/d3m) for more details.

## Examples

Examples of primitives using the unified interface can be found
[in this repository](https://gitlab.com/datadrivendiscovery/common-primitives).

For a Python 2 example, you can look into the [`MonomialPrimitive`](./tests/test_monomial.py) in tests.
