#!/usr/bin/env python

import sys
import unittest

if sys.version_info.major == 2:
    sys.path.insert(0, './python2')

runner = unittest.TextTestRunner(verbosity=1)

tests = unittest.TestLoader().discover('tests')

if not runner.run(tests).wasSuccessful():
    sys.exit(1)
