# How to release a new version

*A cheat sheet.*

* On `devel` branch:
  * Change a version in `primitive_interfaces/__init__.py` to a version without `rc0`.
  * Change `vNEXT` in `HISTORY.md` to the to-be-released version, with `v` prefix.
  * Commit with message `Bumping version for release.`
  * Wait for CI to generate Python 2 version.
  * `git pull`
* On `master` branch:
  * Merge `devel` into `master` branch: `git merge devel`
  * Wait for CI to generate Python 2 version.
  * `git pull`  
  * Tag with version prefixed with `v`, e.g., for version `2017.9.20`: `git tag v2017.9.20`
  * `git push` & `git push --tags`
* On `devel` branch:
  * Change a version in `primitive_interfaces/__init__.py` to the next day (or next known release date) and append `rc0`.
  * Add a new empty `vNEXT` version on top of `HISTORY.md`.
  * Commit with message `Version bump for development.`
  * `git push`

If there is a need for a patch version to fix a released version on the same day,
use `.postX` prefix, like `2017.9.20.post0`. If more than a day has passed, just
use the new day's version.
