from __future__ import absolute_import, division, print_function, unicode_literals

import json
import unittest
from typing import *
import sys

from d3m import index

import primitive_interfaces
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase


Inputs = float
Outputs = float


Params = NamedTuple('Params', [('a', float)])


class MonomialPrimitive(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    A primitive which fits output = a * input.
    """

    __version__ = '0.1.0'
    __author__ = 'Test team.'
    metadata = {
        'languages': ['python2.7', 'python3.6', 'python3.5'],
        'library': 'primitive_interfaces',
        'source_code': 'https://gitlab.com/datadrivendiscovery/primitive-interfaces',
        'build': [],
        "common_name": "Monomial Fit",
        "algorithm_type": ["Instance Based"],
        "handles_regression": True,
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": [],
        },
    }

    def __init__(self):
        # type: () -> None

        super(MonomialPrimitive, self).__init__()

        self.a = None
        self.training_inputs = None
        self.training_outputs = None
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        if self.a is None:
            raise ValueError("Calling produce before fitting.")

        return [self.a * input for input in inputs]

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return

        if not self.training_inputs or not self.training_inputs:
            raise ValueError("Missing training data.")

        quotients = [output / input for output, input in zip(self.training_outputs, self.training_inputs) if input != 0]
        self.a = sum(quotients) / len(quotients)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(a=self.a)

    def set_params(self, params):
        # type: (Params) -> None
        self.a = params.a


EXPECTED_PRIMITIVE_ANNOTATION_JSON = r"""
    {
        "parameters": [],
        "name": "test_name",
        "algorithm_type": [
            "Instance Based"
        ],
        "version": "0.1.0",
        "description": "A primitive which fits output = a * input.\n\nThis is on purpose Python 2.7 primitive to test support for Python 2.7.",
        "id": "a5ca26e7-63a5-30b7-93c5-0fc83c127214",
        "library": "primitive_interfaces",
        "build": [],
        "original_name": "test_monomial.MonomialPrimitive",
        "team": "Test team.",
        "author": "Test team.",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "is_class": true,
        "compute_resources": {
            "gpus_per_node": [],
            "mem_per_node": [],
            "num_nodes": [],
            "expected_running_time": [],
            "sample_size": [],
            "disk_per_node": [],
            "cores_per_node": [],
            "sample_unit": [],
            "mem_per_gpu": []
        },
        "languages": [
            "python2.7",
            "python3.6",
            "python3.5"
        ],
        "methods_available": [
            {
                "description": "",
                "id": "test_name.fit",
                "name": "fit",
                "parameters": [
                    {
                        "optional": true,
                        "name": "timeout",
                        "is_hyperparameter": false,
                        "type": "builtins.float",
                        "default": "None"
                    },
                    {
                        "optional": true,
                        "name": "iterations",
                        "is_hyperparameter": false,
                        "type": "builtins.int",
                        "default": "None"
                    }
                ],
                "returns": {
                    "name": "return_value",
                    "type": "builtins.NoneType"
                }
            },
            {
                "description": "Returns metadata about the last ``produce`` or ``fit`` call.\n\nFor ``produce``, ``has_finished`` is ``True`` if the last call to ``produce``\nhas produced the final outputs and a call with more time or more iterations\ncannot get different outputs.\n\nFor ``fit``, ``has_finished`` is ``True`` if a primitive has been fully fitted\non current training data and further calls to ``fit`` are unnecessary and will\nnot change anything. ``False`` means that more iterations can be done (but it\ndoes not necessary mean that more iterations are beneficial).\n\nIf a primitive has iterations internally, then ``iterations_done`` contains\nhow many of those iterations have been made during the last call. If primitive\ndoes not support them, ``iterations_done`` is ``None``.\n\nThe reason why this is a separate call is to make return value from ``produce`` and\n``fit`` simpler. Moreover, not all callers might care about this information and for\nmany primitives a default implementation of this method works.\n\nReturns\n-------\nCallMetadata\n    A named tuple with metadata.",
                "id": "test_name.get_call_metadata",
                "name": "get_call_metadata",
                "parameters": [],
                "returns": {
                    "name": "return_value",
                    "type": "primitive_interfaces.base.CallMetadata"
                }
            },
            {
                "description": "",
                "id": "test_name.get_params",
                "name": "get_params",
                "parameters": [],
                "returns": {
                    "name": "return_value",
                    "type": "test_monomial.Params"
                }
            },
            {
                "description": "",
                "id": "test_name.produce",
                "name": "produce",
                "parameters": [
                    {
                        "optional": false,
                        "name": "inputs",
                        "is_hyperparameter": false,
                        "type": "builtins.float"
                    },
                    {
                        "optional": true,
                        "name": "timeout",
                        "is_hyperparameter": false,
                        "type": "builtins.float",
                        "default": "None"
                    },
                    {
                        "optional": true,
                        "name": "iterations",
                        "is_hyperparameter": false,
                        "type": "builtins.int",
                        "default": "None"
                    }
                ],
                "returns": {
                    "name": "return_value",
                    "type": "builtins.float"
                }
            },
            {
                "description": "",
                "id": "test_name.set_params",
                "name": "set_params",
                "parameters": [
                    {
                        "optional": false,
                        "name": "params",
                        "is_hyperparameter": false,
                        "type": "test_monomial.Params"
                    }
                ],
                "returns": {
                    "name": "return_value",
                    "type": "builtins.NoneType"
                }
            },
            {
                "description": "Sets a random seed for all operations from now on inside the primitive.\n\nBy default it sets numpy's and Python's random seed.\n\nParameters\n----------\nseed : int\n    A random seed to use.",
                "id": "test_name.set_random_seed",
                "name": "set_random_seed",
                "parameters": [
                    {
                        "optional": false,
                        "name": "seed",
                        "is_hyperparameter": false,
                        "type": "builtins.int"
                    }
                ],
                "returns": {
                    "name": "return_value",
                    "type": "builtins.NoneType"
                }
            },
            {
                "description": "",
                "id": "test_name.set_training_data",
                "name": "set_training_data",
                "parameters": [
                    {
                        "optional": false,
                        "name": "inputs",
                        "is_hyperparameter": false,
                        "type": "builtins.float"
                    },
                    {
                        "optional": false,
                        "name": "outputs",
                        "is_hyperparameter": false,
                        "type": "builtins.float"
                    }
                ],
                "returns": {
                    "name": "return_value",
                    "type": "builtins.NoneType"
                }
            }
        ],
        "returns": {
            "name": "return_value",
            "type": "builtins.NoneType"
        },
        "schema_version": 1.0,
        "handles_regression": true,
        "source_code": "https://gitlab.com/datadrivendiscovery/primitive-interfaceshttps://gitlab.com/datadrivendiscovery/primitive-interfaces",
        "interfaces_version": "__INTERFACES_VERSION__",
        "common_name": "Monomial Fit"
    }
""".replace('__INTERFACES_VERSION__', primitive_interfaces.__version__)


class TestMonomialPrimitive(unittest.TestCase):
    def test_basic(self):
        primitive = MonomialPrimitive()

        primitive.set_training_data(inputs=[1, 2, 3, 4, 5, 6], outputs=[2, 4, 6, 8, 10, 12])
        primitive.fit()
        self.assertEqual(primitive.get_call_metadata().has_finished, True)
        self.assertEqual(primitive.get_call_metadata().iterations_done, None)

        self.assertSequenceEqual(primitive.produce(inputs=[10, 20, 30]), [20, 40, 60])
        self.assertEqual(primitive.get_call_metadata().has_finished, True)
        self.assertEqual(primitive.get_call_metadata().iterations_done, None)

    def test_recreation(self):
        primitive = MonomialPrimitive()

        primitive.set_training_data(inputs=[1, 2, 3, 4, 5, 6], outputs=[2, 4, 6, 8, 10, 12])
        primitive.fit()
        self.assertEqual(primitive.get_call_metadata().has_finished, True)
        self.assertEqual(primitive.get_call_metadata().iterations_done, None)

        params = primitive.get_params()
        primitive = MonomialPrimitive()
        primitive.set_params(params=params)

        self.assertSequenceEqual(primitive.produce(inputs=[10, 20, 30]), [20, 40, 60])
        self.assertEqual(primitive.get_call_metadata().has_finished, True)
        self.assertEqual(primitive.get_call_metadata().iterations_done, None)

    def test_metadata(self):
        expected_annotation_json = EXPECTED_PRIMITIVE_ANNOTATION_JSON.replace('test_monomial', __name__)

        if sys.version_info[0] == 2:
            expected_annotation_json = expected_annotation_json.replace('builtins', '__builtin__')

        expected_annotation = json.loads(expected_annotation_json)

        # We stringify to JSON and parse it to make sure the annotation can be sntringified to JSON.
        annotation = json.loads(json.dumps(index.primitive_to_metadata('test_name', MonomialPrimitive)))

        self.maxDiff = 20000
        self.assertEqual(expected_annotation, annotation)


if __name__ == '__main__':
    unittest.main()
